      <div class="footer">
        <p> <!-- begin disclaimer -->
        © Team Ignite - <a href="http://www.mmu.ac.uk/" title="MMU homepage">Manchester
        Metropolitan University 2014</a> | <a href="http://www.mmu.ac.uk/legal/" title="MMU Legal notice">Legal Notice</a> | <a href="http://www.ico.mmu.ac.uk/feedback.html" title="Feedback form to comment on this web page">Feedback</a> | <a href="http://validator.w3.org/check?uri=referer" title="W3C HTML Validation Service">HTML Validation</a> | <a href="https://bitbucket.org/kvarley/year-2-dip-team-project-website/src" title="View this websites sourcecode on BitBucket.">Sourcecode</a>
      </p>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="assets/vendor/js/bootstrap.min.js"></script>
    <?php
      if($pagename === "Bus Routes") {
    ?>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=geometry"></script>
        <script src="assets/js/map.js"></script>
    <?php
      }
    ?>
  </body>
</html>