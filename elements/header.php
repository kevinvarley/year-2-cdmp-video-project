<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Group 5  | CDMP Documentary student assignment, MMU, UK</title>
    <link href="assets/vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="cdmp">

    <div class="navbar navbar-inverse" role="navigation">
      <div class="container">
      <div class="logo-container">
        <a class="logo-link" href="index.php"><img src="assets/images/mmulogo.png"></a>
      </div>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.html">Home</a></li>
            <li class="active"><a href="project.html">Project Journey</a></li>
            <li class="active"><a href="research.html">Research Findings</a></li>
            <li class="active"><a href="sound.html">Capturing Sound</a></li>
            <li class="active"><a href="filming.html">Filming the Project</a></li>
            <li class="active"><a href="editing.html">Framing the Story</a></li>
            <li class="active"><a href="video.html">Film and Critique</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      <div class="col-md-12">
        <h1>Group 5</h1>
        <h2>Introduction</h2>
        <p>Introduction</p>

        <h2>The Team</h2>

        <div class="well">
          <h3 id="one">Jack Senior</h3>
          <p>description and role here</p>
        </div>

        <div class="well">
          <h3 id="two">Patrick McDermott</h3>
          <p>description and role here</p>
        </div>

        <div class="three">
          <h3 id="one">James Layhe</h3>
          <p>description and role here</p>
        </div>

        <div class="four">
          <h3 id="one">Kevin Varley</h3>
          <p>description and role here</p>
        </div>

        <div class="five">
          <h3 id="one">Francisco</h3>
          <p>description and role here</p>
        </div>
      </div>

      <div class="footer">
        <p> <!-- begin disclaimer -->
        © Team Ignite - <a href="http://www.mmu.ac.uk/" title="MMU homepage">Manchester
        Metropolitan University 2014</a> | <a href="http://www.mmu.ac.uk/legal/" title="MMU Legal notice">Legal Notice</a> | <a href="http://www.ico.mmu.ac.uk/feedback.html" title="Feedback form to comment on this web page">Feedback</a> | <a href="http://validator.w3.org/check?uri=referer" title="W3C HTML Validation Service">HTML Validation</a> | <a href="https://bitbucket.org/kvarley/year-2-dip-team-project-website/src" title="View this websites sourcecode on BitBucket.">Sourcecode</a>
      </p>
      </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="assets/vendor/js/bootstrap.min.js"></script>
  </body>
</html>